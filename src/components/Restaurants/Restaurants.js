import React from 'react';
// import {useState,} from "react";

import usePageData from '../../custom-hooks/usePageData';
import RestaurantCard from '../RestaurantCard/RestaurantCard';
import Spinner from '../Spinner/Spinner';
import CircleLoader from "react-spinners/CircleLoader";

const override = {
  display: "flex",
  justifyContent: "center",
  // width: "100%",
  gridColumnStart: 2,
  margin: "20px auto",
};

const Restaurants = () => {

  const restaurantsList = usePageData('partners');
  console.log(restaurantsList)

  return (
    <div className="restaurants__cards cards">
      {/*<CircleLoader cssOverride={override} color="#36d7b7" size={150} />}*/}

      {restaurantsList
        ? restaurantsList.length
          ? restaurantsList.map(item => {
            return <RestaurantCard key={item.image} {...item}/>
          })
          : <h3>(no items)</h3>
        : <CircleLoader cssOverride={override} color="#36d7b7" size={150}/>}
    </div>
  )
}

export default Restaurants;
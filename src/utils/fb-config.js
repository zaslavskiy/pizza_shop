import firebase from "firebase/compat/app";

// два импорта чтобы все работало)
import 'firebase/compat/auth';
import 'firebase/compat/database';

const firebaseConfig = {
  apiKey: "AIzaSyDZR-mi5khztnJ5WfsZejrXpU0TssK3oiY",
  authDomain: "reactshop-98ee5.firebaseapp.com",
  databaseURL: "https://reactshop-98ee5-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "reactshop-98ee5",
  storageBucket: "reactshop-98ee5.appspot.com",
  messagingSenderId: "1017347179089",
  appId: "1:1017347179089:web:d576f1e446630a9c6c6e70"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
